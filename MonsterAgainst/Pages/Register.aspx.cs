﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using MonsterAgainst.Utility;
using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected bool Validation()
    {
        lblError.Text = "";
        bool validation = true;

        if (string.IsNullOrEmpty(txtName.Text.Trim()))
        {
            lblError.Text = "User name can not empty";
            validation = false;
        }
        UserService us = new UserService();
        if (validation)
        {

            T_User model = us.Where(u => u.Name.Trim().ToLower() == txtName.Text.Trim().ToLower())
                .FirstOrDefault();

            if (model != null)
            {
                lblError.Text = "User name already exists,please use another user name.";
                validation = false;
            }

        }

        if (validation)
        { 
            T_User model = us.Where(u => u.Email.Trim().ToLower() == txtEmail.Text.Trim().ToLower())
                .FirstOrDefault();
            if (model != null)
            {
                lblError.Text = "Email already exists";
                validation = false;
            }
        }

        return validation;
    }

    protected void btnCheck_Click(object sender, EventArgs e)
    {
        try
        {
            if (!Validation()) return;

            T_User model = new T_User()
            {
                UserID = Guid.NewGuid().ToString(),
                Name = txtName.Text.Trim(),
                NickName = txtNickName.Text.Trim(),
                Password = txtPwd.Text,
                Email = txtEmail.Text.Trim(),
                ParentsEmail = txtParentsEmail.Text.Trim()
            };

            UserService service = new UserService();
            service.Insert(model);
            service.Save();

            Session["user"] = model;
            Response.Redirect("~/Default.aspx");
        }
        catch (Exception ex)
        {
            ClientScriptExtension.ShowMessage(this, ex.Message);
        }
    }
}