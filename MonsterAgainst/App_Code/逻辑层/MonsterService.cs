﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Xml.Linq;
using System.Configuration;

using MonsterAgainst.Entity;

namespace MonsterAgainst.BLL
{
    public class LevelItem
    {
        public int MinExperience { get; set; }

        public int MaxExperience { get; set; }

        public string LevelName { get; set; }

        public bool IsLimit { get; set; }

        public int LimitExperience { get; set; }
    }

    public class MonsterService : _BaseService<T_Monster>
    {
        public static Dictionary<string, string> MonsterClass = new Dictionary<string, string>()
        {
            {"wind","wind"},
            {"water","water"},
            {"fire","fire"},
            {"soil","soil"}
        };

        private XDocument docExperience;
        private List<LevelItem> levelItems;

        public MonsterService()
        {

        }

        public MonsterService(bool loadExperience)
        {
            if (loadExperience)
            {
                docExperience = XDocument.Load(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ExperienceDocument"]));

                ParseLevel();
            }
        }

        private void ParseLevel()
        {
            if (docExperience != null)
            {
                levelItems = new List<LevelItem>();
                foreach (XElement eleItem in docExperience.Root.Elements("item"))
                {
                    levelItems.Add(new LevelItem()
                    {
                        MinExperience = int.Parse(eleItem.Attribute("min").Value),
                        MaxExperience = int.Parse(eleItem.Attribute("max").Value),
                        LevelName = eleItem.Value
                    });
                }

                XElement eleLimit = docExperience.Root.Element("limit");
                levelItems.Add(new LevelItem()
                {
                    IsLimit = true,
                    LimitExperience = int.Parse(eleLimit.Attribute("value").Value),
                    LevelName = eleLimit.Value
                });
            }
        }

        public DataTable GetTopPlayer()
        {
            string sql = @"select *,u.Name as UserName from T_Monster m
             left join 
             T_User u
             on m.UserID=u.UserID";

            return ExcuteDataTable(sql);
        }

        public string GetLevel(int experience)
        {
            if (docExperience == null) return "";

            LevelItem limit = levelItems.FirstOrDefault(l => l.IsLimit);
            if (experience >= limit.LimitExperience)
            {
                return limit.LevelName;
            }

            string levelName = string.Empty;
            foreach (LevelItem item in levelItems)
            {
                if (item.IsLimit) continue;

                if (experience == item.MinExperience
                    || experience == item.MaxExperience
                    || (experience > item.MinExperience && experience < item.MaxExperience))
                {
                    levelName = item.LevelName;
                    break;
                }
            }

            return levelName;
        }
    }
}
