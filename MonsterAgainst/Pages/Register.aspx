﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="Script/jquery.js" type="text/javascript"></script>
    <title>Register</title>
    <style type="text/css">
        .auto-style2
        {
            width: 150px;
        }
        .auto-style3
        {
            width: 151px;
        }
        .auto-style4
        {
            width: 158px;
        }
        .auto-style5
        {
            width: 100px;
        }
    </style>
    <script type="text/javascript"> 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 548px">
            <tr>
                <td align="center" colspan="2">
                    User information
                </td>
                <td class="auto-style2">
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    Name<span style="color: Red;">*</span>：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtName" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style10">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                        ErrorMessage="name can't be empty! " SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    NickName<span style="color: Red;">*</span>：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtNickName" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style10">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                        ErrorMessage="Nickname can't be empty! " SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr style="color: #000000">
                <td style="font-size: 9pt;" class="auto-style5">
                    Password<span style="color: Red;">*</span>：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    confirm password<span style="color: Red;">*</span>：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtRePwd" runat="server" TextMode="Password" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPwd"
                        ControlToValidate="txtRePwd" ErrorMessage="Confirm the password and password do not match!"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    Birth：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtBirth" runat="server" Width="138px" CssClass="date_input"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtBirth"
                        ErrorMessage="Date format is wrong! " Operator="DataTypeCheck" SetFocusOnError="True"
                        Type="Date"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    Email<span style="color: Red;">*</span>：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtEmail" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="Email can't be empty! "></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="format is not correct! " ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    parents Email：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtParentsEmail" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="format is not correct! " ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span style="color: Red;">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </span>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:Button ID="btnCheck" runat="server" Text="Register" Style="font-size: 9pt; height: 19;
                        width: 60;" OnClick="btnCheck_Click" />
                    <input name="reset" type="reset" id="reset" value="delete " style="font-size: 9pt;
                        height: 19; width: 60;">
                </td>
                <td style="font-size: 9pt;" class="auto-style3">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
