﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonsterAgainst.Entity;

namespace MonsterAgainst.BLL
{
    public class UserService : _BaseService<T_User>
    {
        public T_User Login(string userName, string password)
        {
            return Where(m => m.Name == userName && m.Password == password)
                .FirstOrDefault();
        }
    }
}
