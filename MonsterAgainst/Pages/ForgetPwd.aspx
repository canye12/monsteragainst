﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgetPwd.aspx.cs" Inherits="ForgetPwd" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="Script/jquery.js" type="text/javascript"></script>
    <title>Register</title>
    <style type="text/css">
        .auto-style2
        {
            width: 150px;
        }
        .auto-style3
        {
            width: 151px;
        }
        .auto-style4
        {
            width: 158px;
        }
        .auto-style5
        {
            width: 100px;
        }
    </style>
    <script type="text/javascript"> 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 548px">
            <tr>
                <td align="center" colspan="2">
                    Retrieve password
                </td>
                <td class="auto-style2">
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    Name：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtName" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style10">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                        ErrorMessage="name can't be empty! " SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                    Email：
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:TextBox ID="txtEmail" runat="server" Width="138px"></asp:TextBox>
                </td>
                <td style="font-size: 9pt;" class="auto-style2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Email can't be empty! "></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="format is not correct! " ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; color: Red;">
                    <asp:Literal ID="ltrPwd" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td style="font-size: 9pt;" class="auto-style5">
                </td>
                <td style="font-size: 9pt;" class="auto-style4">
                    <asp:Button ID="btnCheck" runat="server" Text="Submit" Style="font-size: 9pt; height: 19;
                        width: 60;" OnClick="btnCheck_Click" />
                    <asp:Button ID="btnLogin" runat="server" Text="Login" Style="font-size: 9pt; height: 19;
                        width: 60;" OnClick="btnLogin_Click" />
                </td>
                <td style="font-size: 9pt;" class="auto-style3">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
