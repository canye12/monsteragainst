﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using MonsterAgainst.Utility;
using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class Pages_AddMonster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindClass();
        }
    }

    protected void BindClass()
    {
        Dictionary<string, string> dicClass = new Dictionary<string, string>()
        {
            {"wind","wind"},
            {"water","water"},
            {"fire","fire"},
            {"soil","soil"},
        };

        List<string> hasClass = (new MonsterService()).Where(m => m.UserID == GlobalVariables.CurUser.UserID)
            .Select(m => m.MonsterClass)
            .ToList();

        ddlClass.DataSource = dicClass.Where(m => !hasClass.Contains(m.Key));
        ddlClass.DataTextField = "Value";
        ddlClass.DataValueField = "Key";
        ddlClass.DataBind();
    }

    protected string UploadPicture()
    {
        string picPath = string.Empty;

        if (fluPic.HasFile)
        {
            string dir = Server.MapPath("~/Upload/monsterPic/");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            string tempDir = Server.MapPath("~/Upload/temp/");
            if (!Directory.Exists(tempDir))
            {
                Directory.CreateDirectory(tempDir);
            }
            string guid = Guid.NewGuid().ToString();
            string fileName = guid + Path.GetExtension(fluPic.FileName);
            string tempFilePath = tempDir + fileName;
            string savePath = dir + fileName;

            //upload to temp
            fluPic.PostedFile.SaveAs(tempFilePath);
            //zoom to save
            FileStream stream = File.Open(tempFilePath, FileMode.Open);
            ImageHandler.ZoomAuto(stream, savePath, 180, 180);
            stream.Close();
            stream.Dispose();

            File.Delete(tempFilePath);

            return "~/Upload/monsterPic/" + fileName;
        }

        return "";
    }

    protected void btnCheck_Click(object sender, EventArgs e)
    {
        if (ddlClass.Items.Count == 0)
        {
            ClientScriptExtension.ShowMessage(this, "");
            return;
        }

        string picPath = UploadPicture();

        MonsterService service = new MonsterService();
        T_Monster model = new T_Monster()
        {
            MonsterID = Guid.NewGuid().ToString(),
            MonsterName = txtName.Text,
            MonsterClass = ddlClass.SelectedValue,
            MonsterPicture = picPath,
            UserID = GlobalVariables.CurUser.UserID
        };

        service.Insert(model);
        service.Save();

        Response.Redirect("~/Pages/InfoMonster.aspx");
    }
}
