﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using MonsterAgainst.Utility;
using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class HuobiHuo1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindData();
        }
    }

    protected void BindData()
    {
        MonsterService service = new MonsterService();
        rptMonster.DataSource = service.GetAll().Where(m => m.UserID == GlobalVariables.CurUser.UserID)
            .ToList<T_Monster>();
        rptMonster.DataBind();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        lblError.Text = "";

        if (string.IsNullOrEmpty(hfCheckedMonsterID.Value))
        {
            lblError.Text = "Please choose your monster";
            return;
        }

        //Response.Redirect("HuobiHuo2.aspx?monsterID=" + hfCheckedMonsterID.Value);

        Context.Items.Add("MonsterID", hfCheckedMonsterID.Value);
        Server.Transfer("~/Pages/HuobiHuo2.aspx");
    }
}
