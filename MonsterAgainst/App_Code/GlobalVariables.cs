﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MonsterAgainst.Entity;
 
public class GlobalVariables
{
    /// <summary>
    /// 
    /// </summary>
    public static T_User CurUser
    {
        get
        {
            return (T_User)HttpContext.Current.Session["user"];
        }
    }
}