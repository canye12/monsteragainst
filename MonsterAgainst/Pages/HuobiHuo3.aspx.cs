﻿using System;
using System.Linq;

using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class HuobiHuo3 : System.Web.UI.Page
{
    MonsterService msService = new MonsterService();

    #region attributes

    protected T_Monster Monster
    {
        get
        {
            return msService.Where(m => m.MonsterID == MonsterID)
                .FirstOrDefault();
        }
    }

    protected T_Monster ChallengerMonster
    {
        get
        {
            return msService.Where(m => m.MonsterID == ChallengerMonsterID)
                .FirstOrDefault();
        }
    }

    public string MonsterID
    {
        get { return ViewState["monsterID"] != null ? ViewState["monsterID"].ToString() : string.Empty; }
        set { ViewState["monsterID"] = value; }
    }

    public string ChallengerMonsterID
    {
        get { return ViewState["challengerMonsterID"] != null ? ViewState["challengerMonsterID"].ToString() : string.Empty; }
        set { ViewState["challengerMonsterID"] = value; }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            MonsterID = Session["MonsterID"] != null ? Session["MonsterID"].ToString() : "";
            ChallengerMonsterID = Session["ChallengerMonsterID"] != null ? Session["ChallengerMonsterID"].ToString() : "";

            BindData();
        }
    }

    protected void BindData()
    {
        if (string.IsNullOrEmpty(MonsterID) || string.IsNullOrEmpty(ChallengerMonsterID))
        {
            return;
        }

        if (Monster != null)
        {
            imgMonster.ImageUrl = Monster.MonsterPicture;
            ltrMonsterName.Text = Monster.MonsterName;
        }

        if (ChallengerMonster != null)
        {
            imgChallengerMonster.ImageUrl = ChallengerMonster.MonsterPicture;
            ltrChallengerMonsterName.Text = ChallengerMonster.MonsterName;
        }
    }

    protected int GetIncreaseEXPValue(T_Monster monster)
    {
        int expValue = 100 + (int)(monster.Experience * 0.25);

        return expValue;
    }

    protected void btnBattle_Click(object sender, EventArgs e)
    {
        if (Monster == null || ChallengerMonster == null) return;

        pnlBattle.Visible = false;
        pnlResult.Visible = true;

        long tick = DateTime.Now.Ticks;
        Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));

        int value = ran.Next(10);
        T_User user = null;
        UserService usService = new UserService();
        MonsterService msService = new MonsterService();

        T_Monster winMonster = null;
        if (value < 5)
        {
            winMonster = Monster;
        }
        else//challenger win
        {
            winMonster = ChallengerMonster;
        }

        user = usService.Where(u => u.UserID == winMonster.UserID).FirstOrDefault();
        int increaseExpValue = GetIncreaseEXPValue(winMonster);

        winMonster.Experience += increaseExpValue;
        msService.Update(winMonster);
        msService.Save();

        lblWiner.Text = string.Format("{0}({1})", winMonster.MonsterName, user.Name);
        ltrIncreaseExpValue.Text = increaseExpValue.ToString();
    }
}
