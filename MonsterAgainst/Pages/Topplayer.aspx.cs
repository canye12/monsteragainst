﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MonsterAgainst.BLL;
using MonsterAgainst.Entity;

public partial class Pages_Topplayer : System.Web.UI.Page
{
    MonsterService service = new MonsterService(true);

    protected void Page_Load(object sender, EventArgs e)
    {
        BindData();
    }

    public void BindData()
    {
        grdMonster.DataSource = service.GetTopPlayer();
        grdMonster.DataBind();
    }

    protected void grdMonster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblLevel = e.Row.FindControl("lblLevel") as Label;
        if (lblLevel != null)
        {
            DataRow row = (e.Row.DataItem as DataRowView).Row as DataRow;
            lblLevel.Text = service.GetLevel(int.Parse(row["Experience"].ToString()));
        }
    }
}