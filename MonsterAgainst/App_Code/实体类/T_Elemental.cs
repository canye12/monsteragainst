﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MonsterAgainst.Entity
{
    public class T_Elemental
    {
        [Key]
        [MaxLength(10)]
        public string ElementalID { get; set; }

        [MaxLength(4)]
        public string ElementalClasses { get; set; }
    }
}
