﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MonsterAgainst.Entity
{
    [Serializable]
    public class T_Monster
    {
        [Key]
        [MaxLength(128)]
        public string MonsterID { get; set; }

        [MaxLength(10)]
        public string MonsterName { get; set; }

        public string MonsterClass { get; set; }

        public int Experience { get; set; }

        public string MonsterPicture { get; set; }

        public string UserID { get; set; }
    }
}
