﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InfoMonster.aspx.cs" Inherits="Pages_InfoMonster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:GridView ID="grdMonster" runat="server" AutoGenerateColumns="false" Width="548"
        CssClass="grd" HeaderStyle-CssClass="grdHeader" DataKeyNames="MonsterID" OnRowDataBound="grdMonster_RowDataBound"
        OnRowDeleting="grdMonster_RowDeleting" OnRowCommand="grdMonster_RowCommand">
        <Columns>
            <asp:ImageField DataImageUrlField="MonsterPicture" ControlStyle-Height="100px" ControlStyle-Width="100px"
                ItemStyle-HorizontalAlign="Center">
            </asp:ImageField>
            <asp:BoundField DataField="MonsterName" HeaderText="Monster Name" />
            <asp:BoundField DataField="MonsterClass" HeaderText="Class" />
            <asp:TemplateField HeaderText="Level">
                <ItemTemplate>
                    <asp:Label ID="lblLevel" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="">
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" OnClientClick="return confirm('Are you sure you want to delete the record?')"
                        CommandName="Delete" Text="Delete"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
</asp:Content>
