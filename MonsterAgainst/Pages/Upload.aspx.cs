﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using MonsterAgainst.Utility;

public partial class shangchuan : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (!fluDoc.HasFile) return;
        string dirDoc = Server.MapPath("~/Upload/doc/");

        if (!Directory.Exists(dirDoc))
        {
            Directory.CreateDirectory(dirDoc);
        }

        fluDoc.PostedFile.SaveAs(dirDoc + fluDoc.FileName);
        ClientScriptExtension.ShowMessage(this, "File upload success"); 
    }
}
