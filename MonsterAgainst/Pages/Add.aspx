﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Add.aspx.cs" Inherits="Pages_AddMonster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style2
        {
            width: 150px;
        }
        .auto-style5
        {
            width: 100px;
        }
        .auto-style4
        {
            width: 158px;
        }
        Input
        {
            font-size: 9pt;
        }
        .auto-style3
        {
            width: 151px;
        }
        .auto-style6
        {
            font-family: 宋体;
            font-size: 9pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <table style="width: 548px">
        <tr>
            <td align="center" colspan="2">
                Monster information
            </td>
            <td class="auto-style2">
            </td>
        </tr>
        <tr>
            <td style="font-size: 9pt;" class="auto-style5">
                Name：
            </td>
            <td style="font-size: 9pt;" class="auto-style4">
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
            <td class="auto-style6">
                &nbsp;
            </td>
        </tr>
        <tr style="color: #000000">
            <td style="font-size: 9pt;" class="auto-style5">
                Class
            </td>
            <td style="font-size: 9pt;" class="auto-style4">
                <asp:DropDownList ID="ddlClass" runat="server"> 
                </asp:DropDownList>
            </td>
            <td style="font-size: 9pt;" class="auto-style2">
            </td>
        </tr>
        <tr>
            <td style="font-size: 9pt;" class="auto-style5">
                Picture：
            </td>
            <td style="font-size: 9pt;" class="auto-style4">
                <asp:FileUpload ID="fluPic" runat="server" />
            </td>
            <td class="auto-style6">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="font-size: 9pt;" class="auto-style5">
            </td>
            <td style="font-size: 9pt;" class="auto-style4">
                <asp:Button ID="btnCheck" runat="server" Text="add" Style="font-size: 9pt;" Width="63px"
                    OnClick="btnCheck_Click" />
                <input name="reset" type="reset" id="reset" value="delete " style="font-size: 9pt;
                    height: 19; width: 60;" />
            </td>
            <td style="font-size: 9pt;" class="auto-style3">
            </td>
        </tr>
    </table>
</asp:Content>
