﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

using MonsterAgainst.Entity;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);       
        
        if (Session["User"] == null)
        {
            Response.Write("<script>alert('Please login. ');location='" + ResolveClientUrl("~/") + "Login.aspx'</script>");
            Response.End();
        }
    } 
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["User"] == null)
        {
            Response.Write("<script>alert('Please login. ');location='Login.aspx'</script>");
            return;
            // Response.Redirect("Login.aspx");//go to main page
        }
        lblUserName.Text = (Session["User"] as T_User).Name;
        Label1.Text = "User：" + (Session["User"] as T_User).Name;
    }
}
