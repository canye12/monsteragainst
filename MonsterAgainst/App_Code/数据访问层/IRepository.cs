﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data;

namespace MonsterAgainst.DAL
{
    public interface IRepository<T>
    {
        DbContext DB { get; }

        T GetOne(object key);

        IQueryable<T> GetAll();
        IQueryable<M> GetAll<M>() where M : class;

        IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties);

        IQueryable<T> Where(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetPage(Expression<Func<T, bool>> predicate, int pageIndex, int pageSize, out int count, string sortField, string order = "asc");
        IQueryable<M> GetPage<M>(Expression<Func<M, bool>> predicate, int pageIndex, int pageSize, out int count, string sortField = "id", string order = "asc") where M : class;

        T Insert(T t);

        void Update(T t);

        void Delete(T t);

        void Save();

        DataTable ExcuteDataTable(string sql);
    }
}
