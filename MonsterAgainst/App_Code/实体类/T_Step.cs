﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MonsterAgainst.Entity
{
    public class T_Step
    {
        [Key]
        [MaxLength(10)]
        public string StepID { get; set; }

        public int Step { get; set; }

        public int Level { get; set; }
    }
}
