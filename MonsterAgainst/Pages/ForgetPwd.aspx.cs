﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class ForgetPwd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCheck_Click(object sender, EventArgs e)
    {
        ltrPwd.Text = "";

        UserService service = new UserService();
        T_User model = service.Where(u => u.Name == txtName.Text.Trim() && u.Email == txtEmail.Text.Trim())
              .FirstOrDefault();

        if (model != null)
        {
            ltrPwd.Text = "Your password is \"" + model.Password + "\"";
        }
        else
        {
            ltrPwd.Text = "incorrect password or email";
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Login.aspx");
    }
}