﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class Pages_InfoMonster : System.Web.UI.Page
{
    MonsterService service = new MonsterService(true);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridData();
        }
    }

    protected void BindGridData()
    {
        grdMonster.DataSource = service.GetAll().Where(m => m.UserID == GlobalVariables.CurUser.UserID)
            .ToList();
        grdMonster.DataBind();
    }

    protected void grdMonster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblLevel = e.Row.FindControl("lblLevel") as Label;
        if (lblLevel != null)
        {
            T_Monster model = e.Row.DataItem as T_Monster;
            lblLevel.Text = service.GetLevel(model.Experience);
        }
    }

    protected void grdMonster_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = grdMonster.DataKeys[e.RowIndex].Value.ToString();

        T_Monster model = service.Where(m => m.MonsterID == id).FirstOrDefault();
        if (model != null)
        {
            File.Delete(Server.MapPath(model.MonsterPicture));

            service.Delete(model);
            service.Save();
        }

        BindGridData();
    }

    protected void grdMonster_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }
}