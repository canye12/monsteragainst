﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MonsterAgainst.Entity
{
    public class T_User
    {
        [Key]
        [MaxLength(128)]
        public string UserID { get; set; }

        [MaxLength(10)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string NickName { get; set; }

        [MaxLength(8)]
        public string Password { get; set; }

        public bool Gender { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(100)]
        public string ParentsEmail { get; set; }
    }
}
