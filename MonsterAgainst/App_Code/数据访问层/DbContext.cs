﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

using MonsterAgainst.Entity;

namespace MonsterAgainst.DAL
{
    public class MADbContext : DbContext
    {
        private readonly static string DbName = "connSql";

        public MADbContext()
            : base(MADbContext.DbName)
        {

        }
        
        public DbSet<T_User> Users { get; set; }
        public DbSet<T_Monster> Monsters { get; set; }
        public DbSet<T_Elemental> Elementals { get; set; }
        public DbSet<T_BattleField> BattleFields { get; set; }
        public DbSet<T_Step> Steps { get; set; } 
    }
}
