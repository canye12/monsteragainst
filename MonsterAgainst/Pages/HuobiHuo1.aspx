﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HuobiHuo1.aspx.cs" Inherits="HuobiHuo1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {
            $('.monsterItem').click(function () {
                $('.monsterItem').removeClass('monsterItemSelect');
                $(this).addClass('monsterItemSelect');
            });
        });

        function selectMonster(id) {
            $('input[id$="hfCheckedMonsterID"]').val(id);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <table>
        <tr>
            <asp:Repeater ID="rptMonster" runat="server">
                <ItemTemplate>
                    <td>
                        <div id="<%#Eval("MonsterID") %>" class="monsterItem" onclick="selectMonster('<%#Eval("MonsterID") %>')">
                            <asp:Image runat="server" ImageUrl='<%#Eval("MonsterPicture") %>' Width="180px" Height="180px" />
                            <span class="title">
                                <%#Eval("MonsterName") %>
                            </span>
                        </div>
                    </td>
                </ItemTemplate>
            </asp:Repeater>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" Style="width: 57px" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfCheckedMonsterID" runat="server" />
</asp:Content>
