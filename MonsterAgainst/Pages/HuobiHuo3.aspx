﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HuobiHuo3.aspx.cs" Inherits="HuobiHuo3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <table style="width: 100%; text-align: center;">
        <tr>
            <td style="width: 220px;">
                Monster
                <div class="monsterItem">
                    <asp:Image ID="imgMonster" runat="server" Width="180px" Height="180px" />
                    <span class="title">
                        <asp:Literal ID="ltrMonsterName" runat="server"></asp:Literal>
                    </span>
                </div>
            </td>
            <td>
                <asp:Panel ID="pnlBattle" runat="server">
                    <asp:ImageButton runat="server" ToolTip="Battle" ImageUrl="~/resource/images/battle.jpg" Width="100"
                        Height="100" />
                    <div style="margin-top: 10px;">
                        <asp:Button ID="btnBattle" runat="server" OnClick="btnBattle_Click" Text="Battle"
                            Width="80" CssClass="btn" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlResult" runat="server" Visible="false">
                    Winner: <span style="font-style: italic; font-weight: bold; font-size: 20px;">
                        <asp:Literal ID="lblWiner" runat="server"></asp:Literal>
                    </span>get
                    <asp:Literal ID="ltrIncreaseExpValue" runat="server"></asp:Literal>EXP
                </asp:Panel>
            </td>
            <td style="width: 220px;">
                Challenger's monster
                <div class="monsterItem">
                    <asp:Image ID="imgChallengerMonster" runat="server" Width="180px" Height="180px" />
                    <span class="title">
                        <asp:Literal ID="ltrChallengerMonsterName" runat="server"></asp:Literal>
                    </span>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
