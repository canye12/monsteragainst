﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HuobiHuo2.aspx.cs" Inherits="HuobiHuo2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(function () {
            $('#challengerMonster .monsterItem').click(function () {
                $('#challengerMonster .monsterItem').removeClass('monsterItemSelect');
                $(this).addClass('monsterItemSelect');
            });
        });

        function selectMonster(id) {
            $('input[id$="hfChallengerMonsterID"]').val(id);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <div class="monsterItem">
        <asp:Image ID="imgMonster" runat="server" Width="180px" Height="180px" />
        <span class="title">
            <asp:Literal ID="ltrMonsterName" runat="server"></asp:Literal>
        </span>
    </div>
    <table>
        <tr>
            <td>
                Challenger
                <asp:DropDownList ID="ddlChallenger" runat="server" AutoPostBack="True" Height="16px"
                    OnSelectedIndexChanged="ddlChallenger_SelectedIndexChanged" Width="168px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    Select Challenger Monster
                </div>
                <table id="challengerMonster">
                    <tr>
                        <asp:Repeater ID="rptMonster" runat="server">
                            <ItemTemplate>
                                <td>
                                    <div id="<%#Eval("MonsterID") %>" class="monsterItem" onclick="selectMonster('<%#Eval("MonsterID") %>')">
                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%#Eval("MonsterPicture") %>' Width="180px"
                                            Height="180px" />
                                        <span class="title">
                                            <%#Eval("MonsterName") %>
                                        </span>
                                    </div>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" Style="width: 57px" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfChallengerMonsterID" runat="server" />
</asp:Content>
