﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;

using MonsterAgainst.Utility;

namespace MonsterAgainst.DAL
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected MADbContext db;

        public Repository()
        {
            db = new MADbContext();
        }

        public DbContext DB
        {
            get
            {
                return db;

            }
        }

        public virtual T GetOne(object key)
        {
            return db.Set<T>().Find(key);
        }

        public virtual IQueryable<T> GetAll()
        {
            return db.Set<T>();
        }

        public IQueryable<M> GetAll<M>() where M : class
        {
            return db.Set<M>();
        }

        public virtual IQueryable<T> GetAllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = db.Set<T>();

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public virtual IQueryable<T> Where(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>().Where(predicate);
        }

        public virtual IQueryable<T> GetPage(Expression<Func<T, bool>> predicate, int pageIndex, int pageSize, out int count, string sortField, string order = "asc")
        {
            order = string.IsNullOrEmpty(order) ? "asc" : order.ToLower();

            IQueryable<T> query = db.Set<T>();
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            count = query.Count();

            //query = order == "asc" ? query.OrderBy(sortField) : query.OrderByDescending(sortField);

            query = query.OrderBy(sortField, order);

            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public virtual IQueryable<M> GetPage<M>(Expression<Func<M, bool>> predicate, int pageIndex, int pageSize, out int count, string sortField = "id", string order = "asc") where M : class
        {
            order = string.IsNullOrEmpty(order) ? "asc" : order.ToLower();

            IQueryable<M> query = db.Set<M>();
            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            query = order == "asc" ? query.OrderBy(sortField) : query.OrderByDescending(sortField);

            count = query.Count();
            query = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return query;
        }

        public virtual T Insert(T t)
        {
            return db.Set<T>().Add(t);
        }

        public virtual void Update(T t)
        {
            db.Entry(t).State = System.Data.Entity.EntityState.Modified;
        }

        public virtual void Delete(T t)
        {
            db.Set<T>().Remove(t);
        }

        public virtual void Save()
        {
            db.SaveChanges();
        }

        public virtual DataTable ExcuteDataTable(string sql)
        {
            var sConnection = ((SqlConnection)db.Database.Connection);

            DataTable dt = new DataTable();

            try
            {
                if (sConnection != null && sConnection.State == ConnectionState.Closed)
                {
                    sConnection.Open();
                }
                using (SqlDataAdapter ad = new SqlDataAdapter())
                {
                    SqlDataAdapter com = new SqlDataAdapter(sql, sConnection);
                    com.Fill(dt);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (sConnection != null && sConnection.State == ConnectionState.Closed)
                {
                    sConnection.Close();
                }
            }

            return dt;
        }
    }
}
