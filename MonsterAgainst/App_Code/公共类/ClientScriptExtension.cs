﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace MonsterAgainst.Utility
{
    public class ClientScriptExtension
    {
        public static void ShowMessage(Page page, string message)
        {
            RegisterScriptBlock(page, "alert('" + message + "');");
        }
        public static void RegisterScriptBlock(Page page, string scriptBlock)
        {
            if (string.IsNullOrEmpty(scriptBlock)) return;

            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "ClientScript", scriptBlock, true);
        }
    }
}
