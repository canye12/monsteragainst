﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace MonsterAgainst.Entity
{
    public class T_BattleField
    {
        [Key]
        [MaxLength(10)]
        public string BattleFieldID { get; set; }

        [MaxLength(10)]
        public string BattleFieldName { get; set; }
         
        public DateTime Date { get; set; }

        [MaxLength(10)]
        public string OpponenctID { get; set; }

        [MaxLength(10)]
        public string ChallendgerID { get; set; }

        [MaxLength(10)]
        public string WinnerID { get; set; }
    }
}
