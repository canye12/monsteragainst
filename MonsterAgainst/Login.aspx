<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User login</title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <link rel="stylesheet" href="resource/css/Login_Style.css">
    <style type="text/css">
        .auto-style1
        {
            height: 28px;
        }
    </style>
    <script type="text/javascript">
        function CheckForm() {
        }
    </script>
</head>
<body>
    <p>
    </p>
    <form name="Login" runat="server" method="post" onsubmit="return CheckForm();">
    <table width="585" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="280" rowspan="2">
                <img src="resource/images/entry1.gif" width="280" height="246">
            </td>
            <td width="344" background="resource/images/entry2.gif">
                <table width="100%" border="0" cellspacing="8" cellpadding="0" align="center">
                    <tr align="center">
                        <td height="40" colspan="3">
                            <font color="#000000" size="3"><strong>User Login</strong></font>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="auto-style1">
                            <font color="#000000">UserName:</font>
                        </td>
                        <td class="auto-style1">
                            <input name="UserName" type="text" maxlength="20" style="width: 160px; border-style: solid;
                                border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <font color="#000000">Password:</font>
                        </td>
                        <td>
                            <input name="Password" type="password" maxlength="20" style="width: 160px; border-style: solid;
                                border-width: 1; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div align="center">
                                <asp:Button ID="btnLogin" runat="server" Text="submit" OnClick="btnLogin_Click" Style="font-size: 9pt;
                                    height: 19; width: 60;" />
                                &nbsp;<input name="reset" type="reset" id="reset" value="delete " style="font-size: 9pt;
                                    height: 19; width: 60;"><br>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="3">
            </td>
        </tr>
    </table>
    <p align="center">
        <strong><a href="Pages/Register.aspx">If you are a new user click here to registration</a></strong></font>
        <br>
        <strong><font color="#FF0000"></font></strong><strong><a href="Pages/ForgetPwd.aspx">If you
            forget password click here</a></strong></font>
    </p>
    </form>
</body>
</html>
