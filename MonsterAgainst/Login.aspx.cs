﻿using System;
using System.Configuration;
using System.Data; 
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class _Default : System.Web.UI.Page
{
    UserService service = new UserService();

    protected void Page_Load(object sender, EventArgs e)
    {
    }
     
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        T_User model = service.Login(Request["username"], Request["password"]);
        if (model != null)
        {
            Session["User"] = model;
            Response.Redirect("Default.aspx");//go to main page
        }
        else
        {
            Response.Write("<script>alert('user name or password error,please check. ');location='Login.aspx'</script>");
        }
    }
}


