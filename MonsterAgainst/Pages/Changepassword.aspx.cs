﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MonsterAgainst.Utility;
using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCheck_Click(object sender, EventArgs e)
    {
        T_User user = (T_User)Session["user"];

        if (user.Password != txtOldPwd.Text)
        {
            ClientScriptExtension.ShowMessage(this, "Old password check failed.");
            return;
        }
        if (txtPwd.Text != txtConfirmPwd.Text)
        {
            ClientScriptExtension.ShowMessage(this, "Your new passwords did not match. Please retype your new password in both fields.");
            return;
        }

        user.Password = txtPwd.Text;

        UserService service = new UserService();
        service.Update(user);
        service.Save();

        ClientScriptExtension.ShowMessage(this, "Password changing success！");
    }
}