﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using MonsterAgainst.Entity;
using MonsterAgainst.BLL;

public partial class HuobiHuo2 : System.Web.UI.Page
{

    public string MonsterID
    {
        get { return ViewState["monsterID"] != null ? ViewState["monsterID"].ToString() : string.Empty; }
        set { ViewState["monsterID"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            MonsterID = Context.Items["MonsterID"].ToString();
            Context.Items.Remove("MonsterID");

            BindCheckedMonster();
            BindChallenger();
        }
    }

    protected void BindCheckedMonster()
    {
        T_Monster model = (new MonsterService()).Where(m => m.MonsterID == MonsterID)
            .FirstOrDefault();
        if (model == null) return;

        imgMonster.ImageUrl = model.MonsterPicture;
        ltrMonsterName.Text = model.MonsterName;
    }

    protected void BindChallenger()
    {
        UserService userService = new UserService();
        Dictionary<string, string> dicUser = userService.GetAll().Where(u => u.UserID != GlobalVariables.CurUser.UserID)
                .ToDictionary(u => u.UserID, u => u.Name);

        dicUser = (new Dictionary<string, string>()
        {
            {"","Select Challenger"}
        }).Union(dicUser)
        .ToDictionary(item => item.Key, item => item.Value);

        ddlChallenger.DataSource = dicUser;
        ddlChallenger.DataTextField = "Value";
        ddlChallenger.DataValueField = "Key";
        ddlChallenger.DataBind();
    }

    protected void BindChallengerMonster()
    {
        if (ddlChallenger.Items.Count == 0
            || string.IsNullOrEmpty(ddlChallenger.SelectedValue))
        {
            return;
        }

        MonsterService service = new MonsterService();
        rptMonster.DataSource = service.GetAll()
            .Where(m => m.UserID == ddlChallenger.SelectedValue)
            .ToList<T_Monster>();
        rptMonster.DataBind();
    }

    protected void ddlChallenger_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindChallengerMonster();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        if (string.IsNullOrEmpty(hfChallengerMonsterID.Value))
        {
            lblError.Text = "Please choose challenger monster";
            return;
        }

        Session.Add("MonsterID", MonsterID);
        Session.Add("ChallengerMonsterID", hfChallengerMonsterID.Value);
        Response.Redirect("~/Pages/HuobiHuo3.aspx");
    }
}
